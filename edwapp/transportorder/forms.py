#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from wtforms import (Form, SelectField, StringField, PasswordField, BooleanField, TextAreaField, IntegerField, DecimalField, SubmitField, FieldList, FormField, FloatField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired, Length, Email, NoneOf
from edwapp import lazy_gettext
from edwapp.models import Users, TransportPlacesT, GeoCountries
from edwapp.database import db_session


class TransportLoggedUserF(Form):
    userid = IntegerField(lazy_gettext(u'userid'), render_kw={'readonly':'True'})
    name = StringField(lazy_gettext(u'name'), validators=[Length(min=2, message=lazy_gettext(u'errorlength')), ], render_kw={'class':"required"})
    surname = StringField(lazy_gettext(u'lastname'), validators=[Length(min=2, message=lazy_gettext(u'errorlength')), ], render_kw={'class':"required"})
    usertel = StringField(lazy_gettext(u'tel'))
    useremail = EmailField(lazy_gettext(u'email'))
#    roles = SelectField(label=lazy_gettext(u'userroles'), validators=[NoneOf([-1,], message='error'),], coerce=int, 
#        choices=[(-1, lazy_gettext(u'pleasechoose')),] + \
#                [(f.userid, f.roles.rolename) for f in db_session.query(Users).joinfilter(TransportPlacesT.deleted==0).order_by(TransportPlacesT.friendlyname)])

    active = BooleanField(lazy_gettext(u'active'))
    deleted = BooleanField(lazy_gettext(u'delete'))
    submit = SubmitField(lazy_gettext(u'submit'), render_kw={'class':'commonSubmit'})


class OrderItemsF(Form):
    description = StringField(lazy_gettext(u'itemdescription'), render_kw={"class": "required modalfirst"})
    pieces = IntegerField(lazy_gettext(u'pieces'), render_kw={"class": "required"})
    sizewidth = StringField(lazy_gettext(u'width_m'), render_kw={"class": "required"})
    sizelength = StringField(lazy_gettext(u'length_m'), render_kw={"class": "required"})
    sizeheight = StringField(lazy_gettext(u'height_m'), render_kw={"class": "required"})
    weight = IntegerField(lazy_gettext(u'weight_kg'), render_kw={"class": "required"})
    goodsType = SelectField(label=lazy_gettext(u'goodsType'), choices=[(-1, lazy_gettext(u'pleasechoose')),
                                                                       ('0', lazy_gettext(u'pallet')),
                                                                       ('1', lazy_gettext(u'box')),
                                                                       ('2', lazy_gettext(u'loose'))], render_kw={"class": "required"})
    stackable = BooleanField(label=lazy_gettext(u'stackable'))
    

class DynamicSelectField(SelectField):
    '''https://github.com/wtforms/wtforms/issues/434
    If a form is submitted and the value of a SelectField is an option that was added by Javascript, WTForms will give a 'Not a valid choice' error message.
    This is workaround'''
    def pre_validate(self, form):
        pass


class TransportationWinnerF(Form):
    orderid = IntegerField(lazy_gettext(u'orderid'), render_kw = {'readonly':'True'})
    orderTitle = StringField(lazy_gettext(u'ordertitle'), render_kw = {'readonly':'True'})
    winnerEnteredBy = SelectField(label=lazy_gettext(u'winnerenteredby'), coerce = int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))]) 
    transportWinner = SelectField(label=lazy_gettext(u'winner'), coerce = int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    price = FloatField(lazy_gettext(u'price'))
    currency = SelectField(label=lazy_gettext(u'currency'), coerce = str,
        choices = [('-1', lazy_gettext(u'pleasechoose'))], default = '-1')
    #submit = ButtonField(lazy_gettext(u'submit'), render_kw = {'class':'commonSubmit'})


class TransportOrderCancelationF(Form):
    orderid = IntegerField(lazy_gettext(u'orderid'), render_kw = {'readonly':'True'})
    orderTitle = StringField(lazy_gettext(u'ordertitle'), render_kw = {'readonly':'True'})
    canceledby = SelectField(label=lazy_gettext(u'canceledby'), coerce = int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))]) 
    cancelationReason = TextAreaField(label=lazy_gettext(u'cancelationReason'))
    #submit = ButtonField(lazy_gettext(u'submit'), render_kw = {'class':'commonSubmit'})


class TransportOrderApprovalF(Form):
    orderid = IntegerField(lazy_gettext(u'orderid'), render_kw = {'readonly':'True'})
    orderTitle = StringField(lazy_gettext(u'ordertitle'), render_kw = {'readonly':'True'})
    approvedby = SelectField(label=lazy_gettext(u'approvedby'), coerce = int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))]) 
    #submit = ButtonField(lazy_gettext(u'submit'), render_kw = {'class':'commonSubmit'})


class TransportOrderF(Form):  
    orderTitle = StringField(lazy_gettext(u'ordertitle'), validators=[Length(min=1, message='toto pole musi byt vyplnene'),], render_kw={"class": "first", 'autofocus': True})
    orderUser = SelectField(label=lazy_gettext(u'orderuser'), validators=[NoneOf([-1,], message='error'),], coerce=int, 
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)                
    pickupPlace = SelectField(label=lazy_gettext(u'pickupplace'), validators=[NoneOf([-1,], message='error'),], coerce=int, 
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1) 
    pickupDate = StringField(lazy_gettext(u'pickupdate'), validators=[Length(min=10, message='toto pole musi byt vyplnene'),], render_kw={"class": "datepicker", 'readonly': True})
    pickupContact = DynamicSelectField(label=lazy_gettext(u'pickupcontact'), choices=[(-1, '')])
    deliveryPlace = SelectField(label=lazy_gettext(u'deliveryplace'), validators=[NoneOf([-1,], message='error'),], coerce=int, 
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)                
    deliveryDate = StringField(lazy_gettext(u'deliverydate'), validators=[Length(min=10, message='toto pole musi byt vyplnene'),], render_kw={"class": "datepicker", 'readonly': True})
    deliveryContact = DynamicSelectField(label=lazy_gettext(u'deliverycontact'), choices=[(-1, '')])
    tableofitems = FieldList(FormField(OrderItemsF))
    orderNotes = TextAreaField(label=lazy_gettext(u'ordernotes'))
    submit = SubmitField(label=lazy_gettext(u'addneworder'), render_kw={'class':'commonSubmit'})


class TransportPlacesF(Form):
    placeid = IntegerField(lazy_gettext(u'placeid'), render_kw={'readonly':'True'})
    friendlyname = StringField(lazy_gettext(u'placefriendlyname'), validators=[InputRequired(),])    
    name = StringField(lazy_gettext(u'placename'), validators=[InputRequired(),])
    name2 = StringField(lazy_gettext(u'placename2'))
    address1 = StringField(lazy_gettext(u'address1'), validators=[InputRequired(),])
    streetnumber = StringField(lazy_gettext(u'streetnumber'))
    address2 = StringField(lazy_gettext(u'address2'))
    area = StringField(lazy_gettext(u'area'))
    city = StringField(lazy_gettext(u'city'), validators=[InputRequired(),])
    zip = StringField(lazy_gettext(u'zip'), validators=[InputRequired(),])
    country =  SelectField(label=lazy_gettext(u'selectcountry'), validators=[NoneOf([-1,], message='error'),], coerce=int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    haulier = BooleanField(lazy_gettext(u'hauler'))
    deleted = BooleanField(lazy_gettext(u'deleted'))
    submit = SubmitField(lazy_gettext(u'submit'), render_kw={'class':'commonSubmit'})
