# -*- coding: utf-8 -*-

from datetime import datetime
from flask import render_template, redirect, url_for, request, abort, jsonify, json, flash, current_app, g, make_response
from . import bp
from .forms import OrderItemsF, TransportLoggedUserF, TransportationWinnerF, TransportPlacesF, TransportOrderF, TransportOrderCancelationF, TransportOrderApprovalF
from edwapp import app, login_manager, lazy_gettext
from edwapp.database import db_session
from edwapp.models import (Users, Roles, association_table, TransportPlacesT, users_01, pickPlaces, delPlaces,
                           GeoCountries, GeoContinents, GeoCurrencies, 
                           TransportPlacesContactsT, TransportOrdersT, TransportItemsT)
from edwapp.views import login_required, role_required


#https://pythonise.com/series/learning-flask/flask-message-flashing

@bp.route('/', methods=["POST", "GET"])
def order():
    formorder = TransportOrderF(request.form)
    formitems = OrderItemsF(request.form)
    formorder.orderUser.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted==0).order_by(Users.surname)]
    formorder.pickupPlace.choices += [(f.placeid, f) for f in db_session.query(TransportPlacesT).filter(TransportPlacesT.deleted==0).order_by(TransportPlacesT.friendlyname)]
    formorder.deliveryPlace.choices += [(f.placeid, f) for f in db_session.query(TransportPlacesT).filter(TransportPlacesT.deleted==0).order_by(TransportPlacesT.friendlyname)]
    
    if request.method == 'POST' and formorder.validate():
        if not addNewOrder(formorder, request.form):
            flash(lazy_gettext(u'somethingwentwrong'), 'error')
        else:
            flash(lazy_gettext(u'ordersaved'), 'info')      
        return redirect(url_for('transportorder.order'))    
    g.content['form'] = formorder
    g.content['formitems'] = formitems
    return render_template('order.jinja2', **g.content)


def addNewOrder(objform, req):
    if not objform.tableofitems.entries:
        return False
    order = TransportOrdersT()
    order.orderTitle = objform.orderTitle.data
    order.userid = req.get('orderUser')
    order.pickupPlace = req.get('pickupPlace')
    order.pickupDate = datetime.strptime(objform.pickupDate.data, '%d-%m-%Y')
    order.pickupContactid = req.get('pickupContact')
    order.deliveryPlace = req.get('deliveryPlace')
    order.deliveryDate = datetime.strptime(objform.deliveryDate.data, '%d-%m-%Y')
    order.deliveryContactid = req.get('deliveryContact')
    order.orderNotes = objform.orderNotes.data
    db_session.add(order)
    db_session.commit()
    
    items = TransportItemsT()
    for entry in objform.tableofitems.entries:
        items.orderid = order.orderid
        items.description = entry.data['description']
        items.pieces = entry.data['pieces'] 
        items.sizewidth = entry.data['sizewidth']
        items.sizelength = entry.data['sizelength'] 
        items.sizeheight = entry.data['sizeheight']
        items.weight = entry.data['weight']
        items.goodsType = entry.data['goodsType']
        items.stackable = entry.data['stackable']
    db_session.add(items)
    db_session.commit()
    return True

@bp.route('/orderlist', methods=['POST', 'GET'])
def orderlist():
    form = TransportationWinnerF(request.form)
    form.winnerEnteredBy.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted == 0).order_by(Users.surname)]
    form.transportWinner.choices += [(f.placeid, f) for f in db_session.query(TransportPlacesT).filter(TransportPlacesT.haulier==1).order_by(TransportPlacesT.friendlyname)]
    form.currency.choices += [(f.currencycode, f.currencycode) for f in db_session.query(GeoCurrencies).order_by(GeoCurrencies.currencycode)]


    #if request.method == 'GET' and form.validate():
        #order = TransportOrdersT.query.get(form.orderid.data)
        #order.winnerEnteredBy = request.form.get('winnerEnteredBy')
        #order.winnerEnetredDate = datetime.datetime.now()
        #order.price = form.price.data
        #order.currency = request.form.get('currency')
        #order.transportWinner = request.form.get('transportWinner')
        #db_session.add(order)
        #db_session.commit()
    
    g.content['winnerform'] = form
    g.content['rows'] = db_session.query(TransportOrdersT).join(Users, Users.userid==TransportOrdersT.userid).join(users_01, users_01.userid==TransportOrdersT.approvedby, isouter=True)
    return render_template('orderlist.jinja2', **g.content)


@bp.route('/approveorderlist', methods=['GET', "POST"])
@role_required(roles=['approver', 'superuser'])
def approveorderlist():
    form = TransportOrderApprovalF(request.form)
    g.content['approvalform'] = form
    g.content['rows'] = db_session.query(TransportOrdersT).join(Users, Users.userid==TransportOrdersT.userid).join(users_01, users_01.userid==TransportOrdersT.approvedby, isouter=True)
    return render_template('approvallist.jinja2', **g.content)


@bp.route('/userlist', methods=['GET', "POST"])
def userlist():

    form = TransportLoggedUserF(request.form)
    if request.method == 'POST' and form.validate():
        usr = Users.query.get(form.userid.data)
        usr.name = form.name.data
        usr.surname = form.surname.data
        usr.usertel = form.usertel.data
        usr.useremail = form.useremail.data
        usr.active = form.active.data
        usr.deleted = form.deleted.data
        db_session.add(usr)
        db_session.commit()

    g.content['form'] = form
    g.content['rows'] = db_session.query(Users).join(association_table, isouter=True).join(Roles, isouter=True).filter(Users.deleted==0).order_by(Users.surname)
    return render_template('userlist.jinja2', **g.content)


def getJsonObj(data):
    '''
    convert ORM query object to to dictionary, remove instance, dump to json object,
    which can be passed to jsonify method as ORM object cannot be directly serialized
    '''
    dct = {}
    innerdct = {}
    for e, record in enumerate(data):
        for attr, value in record.__dict__.items():
            if attr in ('deliveryDate', 'pickupDate'):
                innerdct[attr] = value.strftime('%m-%d-%Y')
            else:
                innerdct[attr] = value
        innerdct.pop('_sa_instance_state', None)
        try:
            for attr, value in record.countries.__dict__.items():
                innerdct[attr] = value
            innerdct.pop('_sa_instance_state', None)
        except:
            pass
        try:
            for attr, value in record.users.__dict__.items():
                innerdct[attr] = value
            innerdct.pop('_sa_instance_state', None)
        except:
            pass
        try:
            for attr, value in record.pickplaces.__dict__.items():
                innerdct['pick_' + attr] = value
            innerdct.pop('pick__sa_instance_state', None)
        except:
            pass
        try:
            for attr, value in record.deliveryplaces.__dict__.items():                
                innerdct['del_' + attr] = value
            innerdct.pop('del__sa_instance_state', None)
        except:
            pass
        dct[e] = innerdct
        innerdct = {}
    return json.dumps(dct)


@bp.route('/_getJsonData', methods=['GET',])
def getJsonData():
    '''
    receive clicked object ID and type from js and return json data object back
    '''
    rID = request.args.get('id', 1, type=str)
    rType = request.args.get('type', 2, type=str)
    # option is blank option
    if rID in (-1, '-1'):
        return jsonify(res="")

    if rType == 'user':
        returnData = db_session.query(Users).filter(Users.userid==rID).filter(Users.deleted==0).filter(Users.userid!=1)
    elif rType == 'place':
        returnData = db_session.query(TransportPlacesT).join(GeoCountries).filter(TransportPlacesT.placeid==rID).filter(TransportPlacesT.deleted==0)
    elif rType == 'contacts':
        returnData = db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid==rID).filter(TransportPlacesContactsT.deleted==0)
    elif rType == 'contact':
        returnData = db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.contactid==rID).filter(TransportPlacesContactsT.deleted==0)
    elif rType == 'order':
        returnData = db_session.query(TransportOrdersT).filter(TransportOrdersT.orderid==rID)
    elif rType == 'orderview':
        returnData = db_session.query(TransportOrdersT).join(Users, Users.userid==TransportOrdersT.userid) \
                                                       .join(TransportItemsT, TransportItemsT.orderid==TransportOrdersT.orderid) \
                                                       .join(pickPlaces, pickPlaces.placeid==TransportOrdersT.pickupPlace) \
                                                       .join(delPlaces, delPlaces.placeid==TransportOrdersT.deliveryPlace) \
                                                       .filter(TransportOrdersT.orderid==rID)
        rd = db_session.query(TransportItemsT).filter(TransportItemsT.orderid==rID)

    j = getJsonObj(returnData)
    return jsonify(res=j)


@bp.route('/_confirmvalidation', methods=['POST', 'GET'])
def confirmValidation():
    
    errmessage = json.dumps({'msg':'wrongentryaddagain'})
    if not request.args.get('dsc'):
        return jsonify(res='0')
    try:
        if request.args.get('pcs', type=int) < 1:
            return jsonify(res=errmessage)
        if request.args.get('wi', type=int) < 1:
            return jsonify(res=errmessage)
        if request.args.get('le', type=int) < 1:
            return jsonify(res=errmessage)
        if request.args.get('he', type=int) < 1:
            return jsonify(res=errmessage)
        if request.args.get('wg', type=int) < 1:
            return jsonify(res=errmessage)
    except:
        return jsonify(res=errmessage)
    
    if request.args.get('tp') == -1:
        return jsonify(res=errmessage)
    # success in form
    return jsonify(res='1')


@bp.route('/places', methods=['GET',])
def places():
    form = TransportPlacesF(request.form)
    form.country.choices += [(f.countryid, f) for f in db_session.query(GeoCountries).order_by(GeoCountries.countryname)]

    g.content['form'] = form
    g.content['rows'] = db_session.query(TransportPlacesT).join(GeoCountries).join(GeoContinents).order_by(TransportPlacesT.friendlyname)
    return render_template('places.jinja2', **g.content)


@bp.route('/haulers', methods=['GET',])
def haulers():
    form = TransportPlacesF(request.form)
    g.content['form'] = form
    g.content['rows'] = db_session.query(TransportPlacesT).join(GeoCountries).join(GeoContinents).order_by(TransportPlacesT.friendlyname)
    return render_template('haulers.jinja2', **g.content)