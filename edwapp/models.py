# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import datetime
from sqlalchemy import (Table, Column, String, Integer, DateTime, Text, LargeBinary, CheckConstraint, Boolean, ForeignKey, Float)
from sqlalchemy.orm import relationship, aliased
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from . import database


###################################Observation start#####################################################

class ObservationT(database.Base):
    __tablename__ = 't_observation'
    idobserve = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String(50), nullable=False)
    lastname = Column(String(100), nullable=False)
    date = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    location = Column(String(10), nullable=False)
    safeact = Column(Integer, default=0)
    unsafeact = Column(Integer, default=0)
    unsafecondition = Column(Integer, default=0)
    nearmiss = Column(Integer, default=0)
    qualityfault = Column(Integer, default=0)
    environmentalissue = Column(Integer, default=0)
    observed = Column(Text)
    risclevel = Column(Integer)
    EDW = Column(Integer, default=0)
    guest = Column(Integer, default=0)
    actiontaken = Column(Text)
    escalation = Column(Integer, default=0)
    howandwhoto = Column(Text)
    rootcause = Column(Text)
    chemicalcontact = Column(Integer)
    crushing = Column(Integer)
    cuttingsevering = Column(Integer)
    drawingintrapping = Column(Integer)
    electricity = Column(Integer)
    entanglement = Column(Integer)
    fall = Column(Integer)
    fallingobject = Column(Integer)
    frictionabrasion = Column(Integer)
    hazardousenergyexposure = Column(Integer)
    highpresurefluidinjection = Column(Integer)
    impact = Column(Integer)
    na = Column(Integer)
    noise = Column(Integer)
    shearing = Column(Integer)
    slip = Column(Integer)
    spill = Column(Integer)
    puncture = Column(Integer)
    strain = Column(Integer)
    struck = Column(Integer)
    suffocation = Column(Integer)
    thermaleffect = Column(Integer)
    trip = Column(Integer)
    vibration = Column(Integer)
    materialhandling = Column(Integer)
    ppeusing = Column(Integer)
    waste = Column(Integer)
    nosafetyrules = Column(Integer)
    other = Column(Integer)
    fireprotection = Column(Integer)
    contractor = Column(Integer)
    wrongstandardisation = Column(Integer)
    s5s6 = Column(Integer)
    notsolvedrisk = Column(Integer)
    security = Column(Integer)
    othernotes = Column(Text)

    __table_args__ = (CheckConstraint('risclevel in (0,1,3)'),
                                      {'sqlite_autoincrement': True})


###################################Observation end#######################################################
###################################Pokayoka start########################################################
									  
class PokayokaItems(database.Base):
    __tablename__ = 't_pokayokaitems'
    item = Column(String(20), primary_key=True)
    description = Column(String(30), nullable=False)
    picture = Column(LargeBinary)
    deleted = Column(Integer, nullable=False, default=0)
    __table_args__ = (CheckConstraint('deleted in (0,1)'), {})

    def __init__(self, item=None, description=None, picture=None, deleted=0):
        self.item = item
        self.description = description
        self.picture = picture
        self.deleted = deleted

    def __repr__(self):
        return '({},{})'.format(self.item, self.description)


class PokayokaStruc(database.Base):
    __tablename__ = 't_pokayokastruc'
    toplevel = Column(String(20), ForeignKey('t_pokayokaitems.item'), primary_key=True)
    toplevels = relationship('PokayokaItems', foreign_keys=[toplevel])
    component = Column(String(20), ForeignKey('t_pokayokaitems.item'), primary_key=True)
    components = relationship('PokayokaItems', foreign_keys=[component])
    pcs = Column(Integer, nullable=False, default=1)
    location = Column(Integer, ForeignKey('t_pokayokalocations.idloc'))
    locations = relationship('PokayokaLocations')
    deleted = Column(Integer, nullable=False, default=0)

    __table_args__ = (CheckConstraint('pcs > 0'),
                      CheckConstraint('deleted in (0,1)'), {})

    def __init__(self, toplevel=None, component=None, pcs=1, location=None, deleted=0):
        self.toplevel = toplevel
        self.component = component
        self.pcs = pcs
        self.location = location
        self.deleted = deleted

    def __repr__(self):
        return '({},{},{},{})'.format(
            self.toplevel, self.item, self.pcs, self.location)


class PokayokaLocations(database.Base):
    __tablename__ = 't_pokayokalocations'
    idloc = Column(Integer, autoincrement=True, primary_key=True)    
    location = Column(String(12), nullable=False, unique=True)

    def __init__(self, location):
        self.location = location

    def __repr__(self):
        return '({},{})'.format(self.idloc, self.location)

###################################Pokayoka end##########################################################
###################################KnihaPrijmu start#####################################################

class Carriers(database.Base):
    __tablename__ = 't_carriers'
    carrierID = Column(Integer, primary_key=True, autoincrement=True)
    carrierName = Column(String(64), unique=True, nullable=False)
    deleted = Column(Integer, nullable=False, default=0)
    __table_args__ = (CheckConstraint('deleted in (0,1)'),
                      {'sqlite_autoincrement': True})
    def __repr__(self):
        return '<Carrier %r>' % (self.carrierName)


class ParcellTypes(database.Base):
    __tablename__ = 't_parcellTypes'
    parcellTypeID = Column(Integer, primary_key=True, autoincrement=True)
    typeName = Column(String(50), nullable=False, unique=True)
    deleted = Column(Integer, nullable=False, default=0)
    __table_args__ = (CheckConstraint('deleted in (0,1)'),
                      {'sqlite_autoincrement': True})

    def __repr__(self):
        return '<Parcell Type %r>' % (self.typeName)


class Vendors(database.Base):
    __tablename__ = 't_vendors'
    vendorID = Column(Integer, primary_key=True, autoincrement=True)
    vendorName = Column(String(50), nullable=False, unique=True)
    deleted = Column(Integer, nullable=False, default=0)
    __table_args__ = (CheckConstraint('deleted in (0,1)'),
                      {'sqlite_autoincrement': True})

    def __repr__(self):
        return '<Vendor %r>' % (self.vendorName)


class KPusers(database.Base):
    __tablename__ = 't_kpusers'
    userID = Column(String(20), primary_key=True)
    userName = Column(String(50), nullable=False)
    userSurname = Column(String(100), nullable=False)
    mapicsEnabled = Column(Integer, nullable=False, default=0)
    deleted = Column(Integer, nullable=False, default=0)
    __table_args__ = (CheckConstraint('deleted in (0,1)'),
                      CheckConstraint('mapicsEnabled in (0,1)'))

    def __repr__(self):
        return '<Carrier %r>' % (self.carrierName)


class UserCombined(database.Base):
    __tablename__ = 't_userCombined'
    id1 = Column(String(8), ForeignKey('t_kpusers.userID'), primary_key=True)
    id1s = relationship('KPusers', foreign_keys=[id1])
    id2 = Column(String(8), ForeignKey('t_kpusers.userID'), primary_key=True)
    id2s = relationship('KPusers', foreign_keys=[id2])
    

class Receiving(database.Base):	
    __tablename__ = 't_receiving'
    recID = Column(Integer, primary_key=True, autoincrement=True)
    dateTimeDelivery = Column(String(50), nullable=False)
    vendorID  = Column(Integer, ForeignKey('t_vendors.vendorID'), nullable=False)
    vendorIDrel = relationship('Vendors', foreign_keys=[vendorID])
    carrierID = Column(Integer, ForeignKey('t_carriers.carrierID'), nullable=False)
    carrierIDrel = relationship('Carriers', foreign_keys=[carrierID])
    receivedID = Column(Integer, ForeignKey('t_kpusers.userID'), nullable=False)
    receivedIDrel = relationship('KPusers', foreign_keys=[receivedID])
    deliveryNotes = Column(Text)
    pallets = Column(Integer, nullable=False, default=0)
    barrels = Column(Integer, nullable=False, default=0)
    boxes = Column(Integer, nullable=False, default=0)
    envelopes = Column(Integer, nullable=False, default=0)
    weight = Column(Integer, nullable=False, default=0)
    noEuroPallet = Column(Integer, nullable=False, default=0)
    mixPallet = Column(Integer, nullable=False, default=0)
    highPallet = Column(Integer, nullable=False, default=0)
    customsParcell = Column(Integer, nullable=False, default=0)
    personalParcell = Column(Integer, nullable=False, default=0)
    remark = Column(Text)
    troubleSheet = Column(Integer, nullable=False, default=0)
    bookedInSAPID = Column(String(100))
    bookedInSAPDateTime = Column(String(50))
    nonStandardCar = Column(Integer, nullable=False, default=0)
    __table_args__ = (CheckConstraint('pallets >= 0'),
                      CheckConstraint('barrels >= 0'),
                      CheckConstraint('boxes >= 0'),
                      CheckConstraint('envelopes >= 0'),
                      CheckConstraint('weight >= 0'),
                      CheckConstraint('noEuroPallet in (0,1)'),
                      CheckConstraint('mixPallet in (0,1)'),
                      CheckConstraint('highPallet in (0,1)'),
                      CheckConstraint('customsParcell in (0,1)'),
                      CheckConstraint('troubleSheet in (0,1)'),
                      CheckConstraint('nonStandardCar in (0,1)'),
                      {'sqlite_autoincrement': True})
#    FOREIGN KEY (bookedInMapicsID) REFERENCES v_mapicsUsers(id));

###################################KnihaPrijmu end#######################################################
###################################Transport start#######################################################

association_table = Table('t_userroles', database.Base.metadata,
    Column('user_id', Integer, ForeignKey('t_users.userid', ondelete='CASCADE')),
    Column('role_id', Integer, ForeignKey('t_roles.roleid', ondelete='CASCADE'))
)


class Users(UserMixin, database.Base):
    '''
    #https://flask-login.readthedocs.io/en/latest/_modules/flask_login/mixins.html
    '''
    __tablename__ = 't_users'
    __table_args__ = ({'sqlite_autoincrement': True})
    userid = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String(100, collation='NOCASE'), nullable=False)
    surname = Column(String(100, collation='NOCASE'), nullable=False)
    usertel = Column(String(20))    
    useremail = Column(String(255, collation='NOCASE'), unique=True)
    password_hash = Column(String(255))
    active = Column(Boolean(), nullable=False, default=1)
    deleted = Column(Boolean(), nullable=False, default=0)
    roles = relationship('Roles', secondary=association_table, back_populates='users')
    _is_authenticated = True
    _is_anonymous = False
        
    def __repr__(self):
        return '{0}, {1}'.format(self.surname, self.name)    

    def set_password(self, password):
        self.password_hash = generate_password_hash(password, method='sha256')

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    def get_id(self):
        try:
            return str(self.userid)
        except AttributeError:
            raise NotImplementedError('No `id` attribute - override `get_id`')
    
    @property
    def is_authenticated(self):
        return self._is_authenticated
    
    @is_authenticated.setter
    def is_authenticated(self, value):
        self._is_authenticated = value
    
    @property
    def is_active(self):
        if self.deleted:
            return False
        return self.active 
    
    @is_active.setter
    def is_active(self, value):
        self.active = value

    @property
    def is_anonymous(self):
        return self._is_anonymous
    
    @is_anonymous.setter
    def is_anonymous(self, value):
        self._is_anonymous = value


class Roles(database.Base):
    __tablename__ = 't_roles'
    __table_args__ = ({'sqlite_autoincrement': True})
    roleid = Column(Integer, autoincrement=True, primary_key=True)
    rolename = Column(String(50), unique=True)
    rolearea = Column(String(50))
    users = relationship('Users', secondary=association_table, back_populates='roles')


class TransportOrdersT(database.Base):
    __tablename__ = 't_transportorders'
    __table_args__ = ({'sqlite_autoincrement': True})
    orderid = Column(Integer, autoincrement=True, primary_key=True)
    orderTitle = Column(String(100), nullable=False)
    userid = Column(Integer, ForeignKey('t_users.userid'), nullable=False)
    users = relationship('Users', foreign_keys=[userid])
    orderCreationDate = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    pickupDate = Column(DateTime, nullable=False)
    pickupPlace = Column(Integer, ForeignKey('t_transportplaces.placeid'), nullable=False)
    pickplaces = relationship('TransportPlacesT', foreign_keys=[pickupPlace])
    pickupContactid = Column(Integer, ForeignKey('t_transportplacescontacts.contactid'))
    pickuplacescontacts = relationship('TransportPlacesContactsT', foreign_keys=[pickupContactid])
    deliveryDate = Column(DateTime, nullable=False)
    deliveryPlace = Column(Integer, ForeignKey('t_transportplaces.placeid'), nullable=False)
    deliveryplaces = relationship('TransportPlacesT', foreign_keys=[deliveryPlace])
    deliveryContactid = Column(Integer, ForeignKey('t_transportplacescontacts.contactid'))
    deliveryplacescontacts = relationship('TransportPlacesContactsT', foreign_keys=[deliveryContactid])
    orderNotes = Column(Text)
    winnerEnteredBy = Column(Integer, ForeignKey('t_users.userid'))
    winnerEneters = relationship('Users', foreign_keys=[winnerEnteredBy])
    transportWinner = Column(Integer, ForeignKey('t_transportplaces.placeid'))
    transportWinners = relationship('TransportPlacesT', foreign_keys=[transportWinner])
    winnerEnetredDate = Column(DateTime)
    price = Column(Integer)
    currency = Column(String(3), ForeignKey('t_geocurrencies.currencycode'))
    currencies = relationship('GeoCurrencies', foreign_keys=[currency])
    approvedby = Column(Integer, ForeignKey('t_users.userid'))
    approvers = relationship('Users', foreign_keys=[approvedby])
    orderApprovalDate = Column(DateTime)
    canceledby = Column(Integer, ForeignKey('t_users.userid'))
    cancelators = relationship('Users', foreign_keys=[canceledby])
    cancelationReason = Column(Text) 
    orderCancellationDate = Column(DateTime)


class TransportItemsT(database.Base):
    __tablename__ = 't_transportitems'
    __table_args__ = (CheckConstraint('goodsType in (0,1,2)'), 
                      CheckConstraint('stackable in (0,1)'), 
                      {'sqlite_autoincrement': True})
    itemid = Column(Integer, autoincrement=True, primary_key=True)
    orderid = Column(Integer, ForeignKey('t_transportorders'), nullable=False)
    orders = relationship('TransportOrdersT', backref = 'orderitems')
    description = Column(String(50), nullable=False)
    pieces = Column(Integer, nullable=False)
    sizewidth = Column(Integer, nullable=False)
    sizelength = Column(Integer, nullable=False)
    sizeheight = Column(Integer, nullable=False)
    weight = Column(Integer, nullable=False)
    goodsType = Column(Integer, nullable=False)
    stackable = Column(Integer, nullable=False)


class TransportPlacesT(database.Base):
    __tablename__ = 't_transportplaces'
    __table_args__ = (CheckConstraint('deleted in (0,1)'), CheckConstraint('haulier in (0,1)'),{'sqlite_autoincrement': True})
    placeid = Column(Integer, autoincrement=True, primary_key=True)
    friendlyname = Column(String(100), nullable=False, unique=True)
    name = Column(String(100), nullable=False)
    name2 = Column(String(100))
    address1 = Column(String(120), nullable=False)
    streetnumber = Column(String(20))
    address2 = Column(String(120))    
    area = Column(String(120))
    city = Column(String(120), nullable=False)
    zip = Column(String(20), nullable=False)
    countryid = Column(String(2), ForeignKey('t_geocountries.countryid'), nullable=False)
    countries = relationship('GeoCountries')
    haulier = Column(Integer, nullable=False, default=0)
    deleted = Column(Integer, nullable=False, default=0)
    
    def __repr__(self):
        return '{0}'.format(self.friendlyname)


class TransportPlacesContactsT(database.Base):
    __tablename__ = 't_transportplacescontacts'
    __table_args__ = (CheckConstraint('deleted in (0,1)'), {'sqlite_autoincrement': True})
    contactid = Column(Integer, autoincrement=True, primary_key=True)
    contactname = Column(String(100))
    contacttel = Column(String(100))
    contactemail = Column(String(100))
    placeid = Column(String(2), ForeignKey('t_transportplaces.placeid'), nullable=False)
    places = relationship('TransportPlacesT')    
    deleted = Column(Integer, nullable=False, default=0)

    def __repr__(self):
        return '{0} {1} {2}'.format(self.contactname, self.contacttel, self.contactemail)


class GeoContinents(database.Base):
    __tablename__ = 't_geocontinents'
    continentcode = Column(String(2), primary_key=True)
    continentname = Column(String(50), nullable=False, unique=True)

    def __init__(self, continentcode, continentname):
        self.continentcode = continentcode
        self.continentname = continentname

    def __repr__(self):
        return '({},{})'.format(self.continentcode, self.continentname)


class GeoSubregions(database.Base):
    __tablename__ = 't_geosubregions'
    subregioncode = Column(Integer, primary_key=True)
    subregionname = Column(String(50), nullable=False, unique=True)

    def __init__(self, subregioncode, subregionname):
        self.subregioncode = subregioncode
        self.subregionname = subregionname

    def __repr__(self):
        return '({},{})'.format(self.subregioncode, self.subregionname)
    

class GeoCountries(database.Base):
    __tablename__ = 't_geocountries'
    countryid = Column(Integer, autoincrement=True, primary_key=True)
    countryname = Column(String(80), nullable=False)
    iso3166alpha2 = Column(String(2), nullable=False, unique=True)
    iso3166alpha3 = Column(String(3), nullable=False, unique=True)
    iso3166numeric = Column(Integer, nullable=False, unique=True)
    continentcode = Column(String(2), ForeignKey('t_geocontinents.continentcode'), nullable=False)   
    continents = relationship('GeoContinents')
    subregioncode = Column(Integer, ForeignKey('t_geosubregions.subregioncode'), nullable=False)
    subregions = relationship('GeoSubregions')
    iseu = Column(Integer, nullable=False, default=0)
    isshengen = Column(Integer, nullable=False, default=0)
    
    def __init__(self, countryname, iso3166alpha2, iso3166alpha3, iso3166numeric, continentcode, subregioncode, iseu=0, isshengen=0):
        self.countryname = countryname
        self.iso3166alpha2 = iso3166alpha2
        self.iso3166alpha3 = iso3166alpha3
        self.iso3166numeric = iso3166numeric
        self.continentcode = continentcode
        self.subregioncode = subregioncode
        self.iseu = iseu
        self.isshengen = isshengen

    def __repr__(self):
        return '{0}, {1}'.format(self.countryname, self.iso3166alpha3)


class GeoCurrencies(database.Base):
    __tablename__ = 't_geocurrencies'
    currencycode = Column(String(3), primary_key=True)
    currencyname = Column(String(50), nullable=False, unique=True)

    def __init__(self, currencycode, currencyname):
        self.currencycode = currencycode
        self.currencyname = currencyname

    def __repr__(self):
        return '{}, {}'.format(self.currencycode, self.currencyname)


class ExRates(database.Base):
    __tablename__ = 't_exrates'
    exrateid = Column(Integer, autoincrement=True, primary_key=True)
    exratecurrencycode = Column(String(3), ForeignKey('t_geocurrencies.currencycode'), nullable=False)
    currencycodes = relationship('GeoCurrencies')
    exratevalue = Column(Float(), nullable=False)
    exrateyear = Column(Integer, nullable=False)


users_01 = aliased(Users)
pickPlaces = aliased(TransportPlacesT)
delPlaces = aliased(TransportPlacesT)