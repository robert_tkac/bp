# -*- coding: utf-8 -*-

import os

DEBUG = False
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
SESSION_COOKIE_SAMESITE = 'Strict'
SQLALCHEMY_DATABASE_URI = 'mssql+pymssql://webAppAdmin:webAppAdmin@sqllut002/?charset=utf8'
WTF_CSRF_ENABLED = True
BABEL_TRANSLATION_DIRECTORIES = os.path.join(os.getcwd(), 'translations')
BABEL_DEFAULT_LOCALE = 'cs'
LANGUAGES = {
    'en': 'English',
    'cs': 'Česky'
}
#pokayoka start
POKAYOKA_PRODITEM = ''
POKAYOKA_PRODESCR = ''
POKAYOKA_TOTAL = 0
POKAYOKA_LEFTOVER = 0
POKAYOKA_SUBDATA = ()
#pokayoka end

