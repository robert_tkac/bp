# -*- coding: utf-8 -*-
import os
from edwapp import app


DEBUG = True
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = False
SESSION_COOKIE_SAMESITE = 'Strict'
SECRET_KEY = 'Sm9obiBTY2hyb20ga2lja3MgYXNz'
STRIPE_API_KEY = 'SmFjb2IgS2FwbGFuLU1vc3MgaXMgYSBoZXJv'
SQLALCHEMY_DATABASE_URI = '{0}{1}'.format('sqlite:///', os.path.join(os.path.dirname(os.path.normpath(app.root_path)),'edwapp.sqlite'))
WTF_CSRF_ENABLED = True
BABEL_TRANSLATION_DIRECTORIES = os.path.join(os.path.dirname(os.path.normpath(app.root_path)), 'translations')
BABEL_DEFAULT_LOCALE = 'cs'
LANGUAGES = {
    'en': 'English',
    'cs': 'Česky'
}
#pokayoka start
POKAYOKA_PRODITEM = ''
POKAYOKA_PRODESCR = ''
POKAYOKA_TOTAL = 0
POKAYOKA_LEFTOVER = 0
POKAYOKA_SUBDATA = ()
#pokayoka end